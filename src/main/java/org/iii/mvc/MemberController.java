package org.iii.mvc;

import java.util.List;

import org.iii.jpa.Member;
import org.iii.jpa.MemberRepository;
import org.iii.jpa.MemberService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
@RequestMapping("/member")
public class MemberController {
	@Autowired
	private MemberRepository memberRepository;

	@Autowired
	private MemberService memberService;

	@RequestMapping(value = "/list", method = RequestMethod.GET)
	public String MemberList(Model model) {
		List<Member> members = memberRepository.findAll();
		for (Member m : members) {
			System.out.println(m.getName() + " " + m.getAge());
		}
		model.addAttribute("members", members);
		return "member-list";
	}

	@RequestMapping(value = "/add", method = RequestMethod.GET)
	public String AddMember(Model model) {
		Member newMember = new Member();
		newMember.setName("Mary");
		newMember.setAge(28);
		memberService.add(newMember);
		model.addAttribute("member", newMember);
		return "add-member";
	}

}
