package org.iii.aop;

import java.lang.reflect.Method;

import org.springframework.aop.AfterReturningAdvice;

public class ConsoleAfterAdvice implements AfterReturningAdvice {

	@Override
	public void afterReturning(Object returnValue, Method method, Object[] args, Object target) throws Throwable {
		String targetClassName = target.getClass().getName();
		String targetMethod = method.getName();
		System.out.println("After Advice: " + targetClassName + "." + targetMethod + " Exec");

	}

}
