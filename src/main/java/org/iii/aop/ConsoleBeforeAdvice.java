package org.iii.aop;

import java.lang.reflect.Method;

import org.springframework.aop.MethodBeforeAdvice;

public class ConsoleBeforeAdvice implements MethodBeforeAdvice {

	@Override
	public void before(Method method, Object[] arg1, Object target) throws Throwable {
		String targetClassName = target.getClass().getName();
		String targetMethod = method.getName();
		System.out.println("Before Advice: " + targetClassName + "." + targetMethod + " Exec");

	}

}
