package org.iii.aop;

import org.aopalliance.intercept.MethodInterceptor;
import org.aopalliance.intercept.MethodInvocation;

public class ConsoleAroundAdvice implements MethodInterceptor {

	@Override
	public Object invoke(MethodInvocation invocation) throws Throwable {
		long beginTime = System.currentTimeMillis();
		invocation.proceed();
		long endTime = System.currentTimeMillis();
		String targetMethodName = invocation.getMethod().getName();
		System.out.println("Around Advice: " + targetMethodName + " spend: " + (endTime - beginTime) + " ms");
		return null;
	}

}
