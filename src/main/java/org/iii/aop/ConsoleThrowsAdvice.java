package org.iii.aop;

import java.lang.reflect.Method;

import org.springframework.aop.ThrowsAdvice;

public class ConsoleThrowsAdvice implements ThrowsAdvice {

	public void afterThrowing(Method method, Object[] args, Object target, Throwable exceptionClass) {
		String targetClassName = target.getClass().getName();
		String targetMethod = method.getName();
		System.out.println("Throws Advice: " + targetClassName + "." + targetMethod + " Exec");
	}
}
