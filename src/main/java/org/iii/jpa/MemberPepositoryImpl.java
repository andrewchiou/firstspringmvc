package org.iii.jpa;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Repository("MemberRepository")
@Transactional(propagation = Propagation.REQUIRED)
public class MemberPepositoryImpl implements MemberRepository {
	@PersistenceContext
	private EntityManager entityManager;

	@Override
	public void save(Member member) {
		entityManager.persist(member);

	}

	@Override
	public List<Member> findAll() {
		Query query = entityManager.createQuery("SELECT m FROM Member m");
		@SuppressWarnings("unchecked")
		List<Member> memberList = (List<Member>) query.getResultList();
		return memberList;
	}

}
