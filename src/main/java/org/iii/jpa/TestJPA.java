package org.iii.jpa;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;

public class TestJPA {

	public static void main(String[] args) {
		TestJPA test = new TestJPA();
		test.list();
	}

	public void save(Member member) {
		EntityManager entityManager = JPAUtil.getEntityManagerFactory().createEntityManager();
		EntityTransaction etx = entityManager.getTransaction();
		etx.begin();
		entityManager.persist(member);
		etx.commit();
		entityManager.close();
	}

	public Member findById(int id) {
		EntityManager entityManager = JPAUtil.getEntityManagerFactory().createEntityManager();
		EntityTransaction etx = entityManager.getTransaction();
		etx.begin();
		Member member = entityManager.find(Member.class, id);
		etx.commit();
		entityManager.close();
		return member;
	}

	public void list() {
		EntityManager entityManager = JPAUtil.getEntityManagerFactory().createEntityManager();
		EntityTransaction etx = entityManager.getTransaction();
		etx.begin();
		String hql = "FROM Member";
		List<Member> members = entityManager.createQuery(hql, Member.class).getResultList();
		for (Member m : members) {
			System.out.println(m.getName());
		}
		etx.commit();
		entityManager.close();

	}

}
