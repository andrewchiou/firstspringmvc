package org.iii.jpa;

import java.util.List;

public interface MemberRepository {
	void save(Member member);

	List<Member> findAll();
}
