package org.iii.jpa;

import java.util.List;

public interface MemberService {
	void add(Member member);

	List<Member> findAll();
}
