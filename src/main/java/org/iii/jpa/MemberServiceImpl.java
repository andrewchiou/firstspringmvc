package org.iii.jpa;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service("MemberService")
public class MemberServiceImpl implements MemberService {
	@Autowired
	private MemberRepository memberRepository;

	@Override
	public void add(Member member) {
		memberRepository.save(member);

	}

	@Override
	public List<Member> findAll() {
		return memberRepository.findAll();
	}

}
