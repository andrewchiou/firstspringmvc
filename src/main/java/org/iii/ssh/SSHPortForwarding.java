package org.iii.ssh;

import java.util.Properties;

import com.jcraft.jsch.JSch;
import com.jcraft.jsch.ProxySOCKS5;
import com.jcraft.jsch.Session;

public class SSHPortForwarding {
	private static final int remotePort = 5566;
	private static final String proxyHost = "localhost";
	private static final int proxyPort = 7777;
	private static final String sshServer = "";
	private static final String username = "";
	private static final String password = "";

	public SSHPortForwarding() {
		super();
		try {
			JSch jsch = new JSch();
			Session session = jsch.getSession(username, sshServer, 22);
			session.setPassword(password);

			// Avoid asking for key confirmation
			Properties prop = new Properties();
			prop.put("StrictHostKeyChecking", "no");
			session.setConfig(prop);

			session.setProxy(new ProxySOCKS5(proxyHost, proxyPort));
			session.connect();

			session.setPortForwardingR(remotePort, proxyHost, proxyPort);
		} catch (Exception e) {

		}
	}

	public static void main(String[] args) {
		new SSHPortForwarding();

	}

}
