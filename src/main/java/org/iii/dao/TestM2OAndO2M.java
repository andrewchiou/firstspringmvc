package org.iii.dao;

public class TestM2OAndO2M extends BaseHibernateDao {

	public static void main(String[] args) {
		TestM2OAndO2M test = new TestM2OAndO2M();
		test.testUpdate();

	}

	public void testAdd_1() {
		School school = new School();
		school.setSchoolName("YZU");
		school.setStudentNumber(2000);
		super.add(school);
	}

	public void testAdd_2() {
		School school = (School) super.get(School.class, new Integer(3));
		Student student = new Student();
		student.setStudentNo("444444");
		student.setScore(70);
		student.setSchool(school);
		super.add(student);
	}

	public void testAdd_3() {
		School school = new School();
		school.setSchoolName("CYCU");
		school.setStudentNumber(8000);
		Student student = new Student();
		student.setStudentNo("555555");
		student.setScore(60);
		school.getStudents().add(student);
		super.add(school);
	}

	public void testAdd_4() {
		School school = new School();
		school.setSchoolName("PU");
		school.setStudentNumber(6000);
		Student student = new Student();
		student.setStudentNo("666666");
		student.setScore(80);
		student.setSchool(school); //
		school.getStudents().add(student);
		super.add(school);

	}

	public void testDelete_1() {
		Student student = (Student) super.get(Student.class, new Integer(7));
		super.delete(student);
	}

	public void testDelete_2() {
		School school = (School) super.get(School.class, new Integer(6));
		super.delete(school);
	}

	public void testUpdate() {
		Student student = (Student) super.get(Student.class, new Integer(6));
		School pu = (School) super.get(School.class, new Integer(5));
		School cycu = (School) super.get(School.class, new Integer(4));
		pu.getStudents().remove(student);
		cycu.getStudents().add(student);
		student.setSchool(cycu);
		super.update(student);
	}
}
