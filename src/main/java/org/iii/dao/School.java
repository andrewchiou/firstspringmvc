package org.iii.dao;

import java.util.HashSet;
import java.util.Set;

public class School implements java.io.Serializable {
	private static final long serialVersionUID = 1L;
	private int id;
	private String schoolName;
	private int studentNumber;
	private Set<Student> students = new HashSet<Student>();

	public School() {

	}

	public School(int id) {
		super();
		this.id = id;
	}

	public School(int id, String schoolName, int studentNumber) {
		super();
		this.id = id;
		this.schoolName = schoolName;
		this.studentNumber = studentNumber;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getSchoolName() {
		return schoolName;
	}

	public void setSchoolName(String schoolName) {
		this.schoolName = schoolName;
	}

	public int getStudentNumber() {
		return studentNumber;
	}

	public void setStudentNumber(int studentNumber) {
		this.studentNumber = studentNumber;
	}

	public Set<Student> getStudents() {
		return students;
	}

	public void setStudents(Set<Student> students) {
		this.students = students;
	}

}
