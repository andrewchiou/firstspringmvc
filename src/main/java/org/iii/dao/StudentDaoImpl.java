package org.iii.dao;

public class StudentDaoImpl extends BaseHibernateDao implements StudentDao {

	@Override
	public void add(Student student) {
		super.add(student);

	}

	@Override
	public Student load(int id) {
		return (Student) super.get(Student.class, id);
	}

	@Override
	public void delete(Student student) {
		super.delete(student);

	}

	@Override
	public void update(Student student) {
		super.update(student);

	}

}
