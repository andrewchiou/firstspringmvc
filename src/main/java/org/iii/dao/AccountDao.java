package org.iii.dao;

public interface AccountDao {
	public void add(Account account);

	public Account load(int id);

	public void delete(Account account);

	public void update(Account account);
}
