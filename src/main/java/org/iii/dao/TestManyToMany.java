package org.iii.dao;

import java.util.Set;

public class TestManyToMany extends BaseHibernateDao {

	public static void main(String[] args) {
		TestManyToMany test = new TestManyToMany();
		test.testDelete();;

	}

	public void testAdd_1() {
		Student s1 = new Student();
		s1.setStudentNo("777777");
		Student s2 = new Student();
		s2.setStudentNo("888888");
		Course c1 = new Course();
		c1.setCourseName("javascript");
		Course c2 = new Course();
		c2.setCourseName("python");
		Course c3 = new Course();
		c3.setCourseName("php");
		Course c4 = new Course();
		c4.setCourseName("java");
		s1.getCourses().add(c1);
		s1.getCourses().add(c2);
		c1.getStudents().add(s1);
		c2.getStudents().add(s1);
		s2.getCourses().add(c1);
		s2.getCourses().add(c3);
		s2.getCourses().add(c4);
		c1.getStudents().add(s2);
		c3.getStudents().add(s2);
		c4.getStudents().add(s2);
		super.add(c1);
		super.add(c2);
		super.add(c3);
		super.add(c4);
		super.add(s1);
		super.add(s2);
	}

	public void testAdd_2() {
		Student newStu = new Student();
		newStu.setStudentNo("999999");
		newStu.setScore(60);
		School school = (School) super.get(School.class, new Integer(1));
		newStu.setSchool(school);
		Course course = (Course) super.get(Course.class, new Integer(5));
		newStu.getCourses().add(course);
		super.add(newStu);
		super.update(course);
	}

	public void testDelete() {
		Student student = (Student) super.get(Student.class, new Integer(12));
		Set<Course> courses = student.getCourses();
		for (Course c : courses) {
			c.getStudents().remove(student);
		}
		super.delete(student);
	}

}
