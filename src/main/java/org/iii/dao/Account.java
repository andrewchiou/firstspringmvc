package org.iii.dao;

public class Account implements java.io.Serializable {
	private static final long serialVersionUID = 1L;
	private int id;
	private String accountNo;
	private int balance;

	public Account() {
	}

	public Account(int id) {
		this.id = id;
	}

	public Account(int id, String accountNo, int balance) {
		this.id = id;
		this.accountNo = accountNo;
		this.balance = balance;
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getAccountNo() {
		return this.accountNo;
	}

	public void setAccountNo(String accountNo) {
		this.accountNo = accountNo;
	}

	public int getBalance() {
		return this.balance;
	}

	public void setBalance(int balance) {
		this.balance = balance;
	}

}
