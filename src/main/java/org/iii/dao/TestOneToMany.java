package org.iii.dao;

import java.util.Set;

public class TestOneToMany extends BaseHibernateDao {

	public void get(){
		School school = (School)super.get(School.class, new Integer(1));
		Set<Student> students = school.getStudents();
		for(Student s : students){
			System.out.println(s.getStudentNo());
		}
	}
	
	public static void main(String[] args) {
		TestOneToMany test = new TestOneToMany();
		test.get();

	}

}
