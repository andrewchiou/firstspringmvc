package org.iii.dao;

import java.util.HashSet;
import java.util.Set;

public class Student implements java.io.Serializable {
	private static final long serialVersionUID = 1L;
	private int id;
	private String studentNo;
	private int score;
	private School school;
	private Set<Course> courses = new HashSet<Course>();

	public Student() {
	}

	public Student(int id) {
		this.id = id;
	}

	public Student(int id, String studentNo, int score) {
		this.id = id;
		this.studentNo = studentNo;
		this.score = score;
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getStudentNo() {
		return studentNo;
	}

	public void setStudentNo(String studentNo) {
		this.studentNo = studentNo;
	}

	public int getScore() {
		return score;
	}

	public void setScore(int score) {
		this.score = score;
	}

	public School getSchool() {
		return school;
	}

	public void setSchool(School school) {
		this.school = school;
	}

	public Set<Course> getCourses() {
		return courses;
	}

	public void setCourses(Set<Course> courses) {
		this.courses = courses;
	}

	

}
