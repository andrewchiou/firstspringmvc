package org.iii.dao;

public class TestManyToOne extends BaseHibernateDao {

	public void get() {
		Student student = (Student) super.get(Student.class, new Integer(1));
		System.out.println(student.getSchool().getSchoolName());
	}

	public static void main(String[] args) {
		TestManyToOne test = new TestManyToOne();
		test.get();
	}

}
