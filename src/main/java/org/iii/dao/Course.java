package org.iii.dao;

import java.util.HashSet;
import java.util.Set;

public class Course implements java.io.Serializable {
	private static final long serialVersionUID = 1L;
	private int id;
	private String courseName;
	private Set<Student> students = new HashSet<Student>();

	public Course() {

	}

	public Course(int id) {
		super();
		this.id = id;
	}

	public Course(int id, String courseName, Set<Student> students) {
		super();
		this.id = id;
		this.courseName = courseName;
		this.students = students;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getCourseName() {
		return courseName;
	}

	public void setCourseName(String courseName) {
		this.courseName = courseName;
	}

	public Set<Student> getStudents() {
		return students;
	}

	public void setStudents(Set<Student> students) {
		this.students = students;
	}

}
