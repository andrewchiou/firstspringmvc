package org.iii.dao;

public interface StudentDao {
	public void add(Student student);

	public Student load(int id);

	public void delete(Student student);

	public void update(Student student);
}
