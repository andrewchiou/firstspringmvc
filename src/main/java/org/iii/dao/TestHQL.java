package org.iii.dao;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.query.Query;

public class TestHQL extends BaseHibernateDao {

	public static void main(String[] args) {
		TestHQL test = new TestHQL();
		test.testHql_6();

	}

	public void testHql_1() {
		Session session = getSessionFactory().openSession();
		String hql = "FROM Student";
		@SuppressWarnings("unchecked")
		Query<Student> query = session.createQuery(hql);
		List<Student> list = query.list();
		for (Student s : list) {
			System.out.println(s.getStudentNo());
		}
	}

	@SuppressWarnings({ "rawtypes" })
	public void testHql_2() {
		Session session = getSessionFactory().openSession();
		String hql = "SELECT s.score FROM Student AS s";
		Query query = session.createQuery(hql);
		List list = query.list();
		for (Object o : list) {
			System.out.println(o);
		}
	}

	public void testHql_3() {
		Session session = getSessionFactory().openSession();
		String hql = "FROM Student AS s WHERE s.studentNo=:studentNo";
		@SuppressWarnings("unchecked")
		Query<Student> query = session.createQuery(hql);
		query.setParameter("studentNo", "666666");
		List<Student> list = query.list();
		for (Student s : list) {
			System.out.println(s.getStudentNo() + " " + s.getScore() + " " + s.getSchool().getSchoolName());
		}
	}

	public void testHql_4() {
		Session session = getSessionFactory().openSession();
		String hql = "SELECT student FROM Student AS student, School AS school WHERE student.school = school AND school.schoolName=:schoolName";
		@SuppressWarnings("unchecked")
		Query<Student> query = session.createQuery(hql);
		query.setParameter("schoolName", "NCU");
		List<Student> list = query.list();
		for (Student s : list) {
			System.out.println(s.getStudentNo() + " " + s.getScore() + " " + s.getSchool().getSchoolName());
		}
	}

	public void testHql_5() {
		Session session = getSessionFactory().openSession();
		String hql = "FROM Student AS s ORDER BY s.score ASC";
		@SuppressWarnings("unchecked")
		Query<Student> query = session.createQuery(hql);
		query.setFirstResult(0);
		query.setMaxResults(3);
		List<Student> list = query.list();
		for (Student s : list) {
			System.out.println(s.getStudentNo());
		}
	}

	public void testHql_6() {
		Session session = getSessionFactory().openSession();
		String hql = "SELECT count(s) FROM Student AS s";
		@SuppressWarnings("rawtypes")
		Query query = session.createQuery(hql);
		Long count = (Long) query.uniqueResult();
		System.out.println(count);
	}

}
