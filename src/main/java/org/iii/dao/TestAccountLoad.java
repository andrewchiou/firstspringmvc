package org.iii.dao;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class TestAccountLoad {

	public static void main(String[] args) {
		Configuration config = new Configuration().configure();
		SessionFactory sessionFactory = config.buildSessionFactory();
		Session session = sessionFactory.openSession();
		Account account = session.get(Account.class, new Integer(1));
		System.out.println(account.getAccountNo() + " " + account.getBalance());

	}

}
