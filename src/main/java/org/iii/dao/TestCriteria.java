package org.iii.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

public class TestCriteria extends BaseHibernateDao {

	public static void main(String[] args) {
		TestCriteria test = new TestCriteria();
		test.testCriteria_1();

	}

	public void testCriteria_1() {
		Session session = getSessionFactory().openSession();
		@SuppressWarnings("deprecation")
		Criteria criteria = session.createCriteria(Student.class).createCriteria("school");
		// change to JPA, use HQL first
		criteria.add(Restrictions.eq("schoolName", "NCU"));
		@SuppressWarnings("unchecked")
		List<Student> list = criteria.list();
		for (Student s : list) {
			System.out.println(s.getStudentNo() + " " + s.getScore() + " " + s.getSchool().getSchoolName());
		}
	}
	
	

}
