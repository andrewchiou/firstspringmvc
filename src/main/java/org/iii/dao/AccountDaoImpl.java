package org.iii.dao;

public class AccountDaoImpl extends BaseHibernateDao implements AccountDao {

	@Override
	public void add(Account account) {
		super.add(account);

	}

	@Override
	public Account load(int id) {
		return (Account) super.get(Account.class, id);
	}

	@Override
	public void delete(Account account) {
		super.delete(account);

	}

	@Override
	public void update(Account account) {
		super.update(account);

	}

}
