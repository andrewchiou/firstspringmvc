package org.iii.biz;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class TestProxyUserBiz {

	public static void main(String[] args) {
		@SuppressWarnings("resource")
		ApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");
		UserBiz userBiz = (UserBiz) context.getBean("PROXY_USER_BIZ");
		userBiz.addUser("Andrew", "123");
		userBiz.delUser(5);

	}

}
