package org.iii.biz;

import org.iii.dao.UserDao;

public class UserBizImpl implements UserBiz {
	UserDao userDao;

	public void setUserDao(UserDao userDao) {
		this.userDao = userDao;
	}

	@Override
	public boolean login(String username, String password) {
		return userDao.login(username, password);
	}

	@Override
	public void addUser(String username, String password) {
		userDao.addUser(username, password);

	}

	@Override
	public void delUser(int id) {
		userDao.delUser(id);

	}

}
