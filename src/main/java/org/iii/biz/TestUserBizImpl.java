package org.iii.biz;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class TestUserBizImpl {

	public static void main(String[] args) {
		@SuppressWarnings("resource")
		ApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");
		UserBiz userBiz = (UserBiz) context.getBean("USER_BIZ");
		System.out.println(userBiz.login("admin", "123"));

	}

}
